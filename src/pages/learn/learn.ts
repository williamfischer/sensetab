import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Vibration } from '@ionic-native/vibration';
import { SpeechRecognition } from '@ionic-native/speech-recognition';

import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { DashboardPage } from '../pages';

/**
 * Generated class for the LearnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-learn',
  templateUrl: 'learn.html'
})
export class LearnPage {

  hitshift: boolean;
  hitcaps: boolean;
  specialkeys: boolean;
  theValue: string = '';

  titlewiki: string;
  extractwiki: any;
  everything: any;
  otherResults: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private vibration: Vibration, private http2: HttpClient, private speechRecognition: SpeechRecognition) {
    this.hitshift = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LearnPage');
  }

  dashTrigger(){
    this.navCtrl.push(DashboardPage);
  }

  backValue(){
    this.theValue = '';
    this.titlewiki = '';
    this.extractwiki = '';
  }

  useKeyboard(key) {
      this.vibration.vibrate(20);

      if (this.hitshift) {
          this.theValue = this.theValue + key.toUpperCase();
      } else {
          this.theValue = this.theValue + key
      }

      if(this.theValue){
        var theValue = this.theValue;
      }else{
        var theValue = '';
      }

      this.WikiSearch(theValue)


      if (this.hitshift && !this.hitcaps) {
          this.hitshift = false
      }
  }

    /** Use backspace */
    useBackspace() {
        this.theValue = this.theValue.substring(0, this.theValue.length - 1);
        if(this.theValue){
          var theValue = this.theValue;
        }else{
          var theValue = '';
        }
        this.WikiSearch(theValue)

    }


    /** Use Shift */
    activateshift() {
        this.vibration.vibrate(20);

        if (this.hitshift) {
            this.hitshift = false
        } else {
            this.hitshift = true
        }

        if(this.hitcaps){
          this.hitcaps = false;
        }
    }

    /** Use Caps */
    activatecaps() {
        this.hitshift = true
        this.hitcaps = true
    }

    /** Activate other icons */
    moreicons() {
        this.vibration.vibrate(20);

        if (this.specialkeys) {
            this.specialkeys = false
        } else {
            this.specialkeys = true
        }
    }

    /** This will activate the Mic */
    activateMic(){
      this.speechRecognition.hasPermission().then((hasPermission: boolean) => {
        if(hasPermission){
          let options = {
            showPopup : false,
            matches : 1,
            prompt : 'Listening...'
          }

          this.speechRecognition.startListening(options).subscribe((matches: Array<string>) => {
            for (var key in matches) {
              this.theValue = matches[key];

              if(this.theValue){
                var theValue = this.theValue;
              }else{
                var theValue = '';
              }

              this.titlewiki = theValue;
              this.WikiSearch(theValue)
            }
          },(onerror) => console.log('error:', onerror))

        }else{
          this.speechRecognition.requestPermission()
          .then(
            () => this.activateMic(),
            () => console.log('Denied')
          )
        }
      })
    }

   /** Searches wikipedia */
    WikiSearch(theValue) {
        console.log(this.theValue)

        interface UserResponse {
            query: any;
            pages: any;
        }

        this.http2.get < UserResponse >
            ('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=images|info|extracts&exintro=&explaintext=&titles=' + theValue + '&redirects=1').subscribe(
                data => {
                    this.extractwiki = data;
                    this.otherResults = data;
                    this.otherResults = Array.of(this.otherResults);
                },
                err => console.log("Error: " + JSON.stringify(err)),
                () => {

                    Object.keys(this.otherResults).forEach(key => {
                        if (this.otherResults[key] && theValue) {
                            var searchAll = this.otherResults[key].query.pages;

                            for (var key in searchAll) {
                                if (searchAll.hasOwnProperty(key)) {
                                    console.log(searchAll[key])
                                    this.titlewiki = searchAll[key].title;
                                    //this.extractwiki = searchAll[key].extract.replace(/\.(?=[^\d])[ ]*/g, '. \n');
                                    this.extractwiki = searchAll[key].extract.replace(/\n/g, ' \n\n');
                                    // this.everything = JSON.stringify(searchAll[key]);
                                }

                            }
                        }
                    });
                }
            );
    }


}
