import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Vibration } from '@ionic-native/vibration';

import { DashboardPage } from '../pages';


@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  critters: any[];
  selectedCritter: string;
  selectedHands: boolean;
  handRight: boolean;
  dashMode: boolean;

  signinFormContinue: boolean;
  setupKeyboard: boolean;

  hitshift: boolean;
  specialkeys: boolean;
  theValue: string = '';

  constructor(public navCtrl: NavController, private vibration: Vibration) {
    this.critters = ['tiger', 'sloth', 'penguin', 'bunny', 'fox', 'glider', 'hedgehog', 'koala', 'lion', 'mouse', 'owl', 'panda', 'possum', 'seal', 'snail', 'squirrl']
  }

  selectCritter(critter) {
      this.selectedCritter = critter
      this.signinFormContinue  = true;
  }

  setupKeyboardMode() {
    this.setupKeyboard = true;
  }

  unSetupKeyboardMode(){
    this.setupKeyboard = false;
  }

  useKeyboard(key) {
      this.vibration.vibrate(20);

      if (this.hitshift) {
          this.theValue = this.theValue + key.toUpperCase();
      } else {
          this.theValue = this.theValue + key
      }

      // var str = this.theValue;
      // var theValue = str.replace(/ /g, "%20");


  }

  useBackspace() {
      this.theValue = this.theValue.substring(0, this.theValue.length - 1);

      // var str = this.theValue;
      // var theValue = str.replace(/ /g, "%20");
  }

  backToHand(){
    this.selectedHands = false;
    this.setupKeyboard = false;
    this.handRight = false;
  }

  leftHandClick(){
    this.selectedHands = true;
    this.handRight = false;
  }

  rightHandClick(){
    this.selectedHands = true;
    this.handRight = true;
  }

  activateshift() {
      this.vibration.vibrate(20);

      if (this.hitshift) {
          this.hitshift = false
      } else {
          this.hitshift = true
      }
  }

  dashTrigger(){
    this.navCtrl.push(DashboardPage);
  }
  moreicons() {
      this.vibration.vibrate(20);

      if (this.specialkeys) {
          this.specialkeys = false
      } else {
          this.specialkeys = true
      }
  }

}
