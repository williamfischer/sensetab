import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BatteryStatus } from '@ionic-native/battery-status';

import { LearnPage } from '../pages';
import { TeachPage } from '../pages';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  batteryLevel: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private batteryStatus: BatteryStatus) {
    this.batteryStatus.onChange().subscribe(status => {
       console.log("BATTERY STATUS IS: " + status.level);
       this.batteryLevel = status.level;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  learnTrigger(){
    this.navCtrl.push(LearnPage);
  }

  teacherTrigger(){
    console.log("GO TEACH MODE")
    this.navCtrl.push(TeachPage);
  }




}
